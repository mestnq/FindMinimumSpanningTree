﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Algorithm
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] matrix = new WorkWithFile().ReadFile();
            if (matrix.GetLength(0) != matrix.GetLength(1))
            {
                throw new Exception("Matrix is wrong");
            }

            Console.WriteLine("Алгоритм Прима:");
            PrimsAlgorithm.Make(matrix); //easy version

            Console.WriteLine("Алгоритм Крускала:");
            KruskalAlgorithm.Make(matrix); //easy version
        }
    }

    public class KruskalAlgorithm
    {
        public static void Make(int[,] matrix)
        {
            List<(int value, int firstTop, int secondTop)> valueAndEdges = SortedEdgesByValue(matrix);
            List<List<int>> plenty = new List<List<int>>(); //множество
            plenty.Add(new List<int>());
            plenty[0].Add(valueAndEdges[0].firstTop);
            Console.WriteLine("Edge \tWeight");
            for (int i = 0; i < valueAndEdges.Count; i++)
            {
                if (!SearchCycle(valueAndEdges[i].firstTop, valueAndEdges[i].secondTop, plenty))
                {
                    AddToPlenty(plenty, valueAndEdges[i].firstTop, valueAndEdges[i].secondTop);
                    Console.WriteLine(valueAndEdges[i].firstTop
                                      + " - " + valueAndEdges[i].secondTop + "\t" + valueAndEdges[i].value);
                }
            }
        }

        private static void AddToPlenty(List<List<int>> plenty, int firstTop, int secondTop)
        {
            foreach (var list1 in plenty)
            {
                foreach (var list2 in plenty)
                {
                    if (list1 != list2)
                    {
                        if (list2.Contains(firstTop) && list1.Contains(secondTop) ||
                            list2.Contains(secondTop) && list1.Contains(firstTop))
                        {
                            list1.InsertRange(list1.Count, list2);
                            list2.Clear();
                            goto LoopEnd; //не бейте
                        }
                    }
                }

                if (list1.Contains(firstTop))
                {
                    list1.Add(secondTop);
                    break;
                }

                if (list1.Contains(secondTop))
                {
                    list1.Add(firstTop);
                    break;
                }

                if (!list1.Contains(secondTop) && !list1.Contains(firstTop))
                {
                    List<int> newList = new List<int> {firstTop, secondTop};
                    plenty.Add(newList);
                    break;
                }

                LoopEnd:
                break;
            }
        }

        private static bool SearchCycle(int firstTop, int secondTop, List<List<int>> plenty)
        {
            foreach (var list in plenty)
            {
                if (list != null && list.Contains(firstTop) && list.Contains(secondTop))
                {
                    return true;
                }
            }

            return false;
        }

        private static List<(int value, int firstTop, int secondTop)> SortedEdgesByValue(int[,] matrix)
        {
            List<(int value, int firstTop, int secondTop)> valueAndEdges =
                new List<(int value, int firstTop, int secondTop)>();
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = i; j < matrix.GetLength(0); j++)
                {
                    if (matrix[i, j] > 0)
                    {
                        valueAndEdges.Add((matrix[i, j], i, j));
                    }
                }
            }
            //var sorted = valueAndEdges.OrderBy(e => e.value);
            //return sorted.ToList();
            return valueAndEdges.OrderBy(e => e.value).ToList();
        }
    }
    
    public class PrimsAlgorithm
    {
        public static void Make(int[,] matrix)
        {
            int v = matrix.GetLength(0);
            int[] parent = new int[v]; //для хранения индексов родительских узлов в MST
            int[] key = new int[v]; //для хранения значений ключей всех вершин
            bool[] mstSet = new bool[v]; //для представления набора вершин, включенных в MST
            for (int i = 0; i < v; i++) //Инициализируем ключи
            {
                key[i] = int.MaxValue;
                mstSet[i] = false;
            }

            key[0] = 0;
            parent[0] = -1;
            for (int count = 0; count < v - 1; count++)
            {
                int u = MinKey(key, mstSet, v);
                mstSet[u] = true;
                for (int f = 0; f < v; f++)
                {
                    if (matrix[u, f] != 0 && mstSet[f] == false && matrix[u, f] < key[f])
                    {
                        parent[f] = u;
                        key[f] = matrix[u, f];
                    }
                }
            }

            PrintMST(parent, matrix, v);
        }

        private static int MinKey(int[] key, bool[] mstSet, int v)
        {
            int min = int.MaxValue;
            int minIndex = -1;
            for (int i = 0; i < v; i++)
            {
                if (mstSet[i] == false && key[i] < min)
                {
                    min = key[i];
                    minIndex = i;
                }
            }

            return minIndex;
        }

        private static void PrintMST(int[] parent, int[,] graph, int v)
        {
            Console.WriteLine("Edge \tWeight");
            for (int i = 1; i < v; i++)
                Console.WriteLine(parent[i] + " - " + i + "\t" + graph[i, parent[i]]);
        }
    }
}