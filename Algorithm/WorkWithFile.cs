﻿using System;
using System.IO;

namespace Algorithm
{
    public class WorkWithFile
    {
        public int[,] ReadFile()
        {
            string[] txt = File.ReadAllLines(@"../../../TheAdjacencyMatrixOfAGraph.txt");
            int[,] matrix = new int[txt.Length, txt.Length];
            for (int i = 0; i < txt.Length; i++)
            { 
                string[] array = txt[i].Split(" ");
                for (int j = 0; j < array.Length; j++)
                {
                    matrix[i, j] = Convert.ToInt32(array[j]);
                }
            }

            return matrix;
        }
    }
}